class AddAtributesIntoUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :first_name, :string, null: false
    add_column :users, :last_name, :string, null: false
    add_column :users, :phone_number, :string, null: true
    add_column :users, :birth_date, :datetime, null: false
    add_column :users, :state, :string, null: false, default: "active"
  end
end
